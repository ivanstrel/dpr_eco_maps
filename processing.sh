########################################################
#	Requirements:
#	  grass-dev package should be installed in system
########################################################

# Create folder
mkdir $HOME/Documents/grass_data/eco_maps/PERMANENT

# Create new project in /eco_maps/ folder basing on S2A_2018_05_29_B02_10m.tif
LOCATION="$HOME/Documents/grass_data/eco_maps/"
grass74 -c /home/ivan/Data/Sentinel_2/DPR_29_05_2018/merged/S2A_2018_05_29_B02_10m.tif $LOCATION

# Import Sentinel B02 channel
r.import input=/home/ivan/Data/Sentinel_2/DPR_29_05_2018/merged/S2A_2018_05_29_B02_10m.tif output=S2A_2018_05_29_B02_10m

# Import ALOS_merged dem data
r.import input=/home/ivan/Data/ALOS_World_3D/merged/ALOS_merged.tif output=ALOS_merged

# Resample ALOS_merged
r.resamp.interp --overwrite input=ALOS_merged@PERMANENT output=ALOS_merged@PERMANENT method="bilinear"



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# hydrological analysis
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Install r.hydrodem addon
g.extension r.hydrodem

r.hydrodem --overwrite input=ALOS_merged@PERMANENT output=ALOS_hydrodem@PERMANENT

# Remove pixels < 0
r.mapcalc --overwrite expression="ALOS_water_masked = if (ALOS_hydrodem@PERMANENT <= 0, null(), ALOS_hydrodem@PERMANENT)"

r.watershed -b --overwrite elevation=ALOS_water_masked@PERMANENT threshold=1000000 accumulation=ALOS_water_accumulation basin=ALOS_water_basin

g.remove -f type=raster name=ALOS_water_masked@PERMANENT
g.remove -f type=raster name=ALOS_hydrodem@PERMANENT

r.mapcalc --overwrite expression="ALOS_water_accumulation_int = round(if((ALOS_water_accumulation * 100) < 0, null(), if((ALOS_water_accumulation * 100) > 65534 , null(), (ALOS_water_accumulation * 100))))"
r.out.gdal -c --overwrite input=ALOS_water_accumulation_int@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/hydrology/ALOS_water_accumulation_int.jp2 format="JP2OpenJPEG" createopt="QUALITY=100, REVERSIBLE=YES YCBCR420=NO" type="UInt16"
g.remove -f type=raster name=ALOS_water_basin@PERMANENT
g.remove -f type=raster name=ALOS_water_accumulation_int@PERMANENT
g.remove -f type=raster name=ALOS_water_accumulation@PERMANENT

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Relief analysis
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
r.mapcalc expression="ALOS_merged_zero_water = if(ALOS_merged <= 0, 0, ALOS_merged)"

# Slope and aspect analysis
r.slope.aspect --overwrite --verbose elevation=ALOS_merged_zero_water@PERMANENT slope=ALOS_slope aspect=ALOS_aspect pcurvature=ALOS_curvature

# Produce smoothed maps
r.neighbors -c --overwrite input=ALOS_aspect@PERMANENT output=ALOS_aspect_smooth_10 method=median size=11
r.neighbors -c --overwrite input=ALOS_slope@PERMANENT output=ALOS_slope_smooth_10 size=11

# Geomorphons computations
r.neighbors -c --overwrite input=ALOS_merged_zero_water@PERMANENT output=ALOS_merged_zero_water_smooth size=11 gauss=5
r.geomorphon --overwrite --verbose elevation=ALOS_merged_zero_water_smooth@PERMANENT forms=ALOS_geomorfon_forms search=5

## Save results and clean up
r.mapcalc --overwrite expression="ALOS_slope_smooth_10_int = round(ALOS_slope_smooth_10 * 100)"
r.out.gdal -c --overwrite input=ALOS_slope_smooth_10_int@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/terrain/ALOS_slope_smooth_10_int.tiff format="GTiff" type="UInt16" createopt="PROFILE=GeoTIFF,TFW=YES"
g.remove -f type=raster name=ALOS_slope_smooth_10_int@PERMANENT

r.mapcalc --overwrite expression="ALOS_slope_int = round(ALOS_slope * 100)"
r.out.gdal -c --overwrite input=ALOS_slope_int@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/terrain/ALOS_slope_int.tiff format="GTiff" type="Int16" createopt="PROFILE=GeoTIFF,TFW=YES"
g.remove -f type=raster name=ALOS_slope_int@PERMANENT

r.mapcalc --overwrite expression="ALOS_aspect_smooth_10_int = round(ALOS_aspect_smooth_10 * 10)"
r.out.gdal -c --overwrite input=ALOS_aspect_smooth_10_int@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/terrain/ALOS_aspect_smooth_10_int.tiff format="GTiff" type="UInt16" createopt="PROFILE=GeoTIFF,TFW=YES"
g.remove -f type=raster name=ALOS_aspect_smooth_10_int@PERMANENT

r.mapcalc --overwrite expression="ALOS_aspect_int = round(ALOS_aspect * 10)"
r.out.gdal -c --overwrite input=ALOS_aspect_smooth_10_int@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/terrain/ALOS_aspect_int.tiff format="GTiff" type="UInt16" createopt="PROFILE=GeoTIFF,TFW=YES"
g.remove -f type=raster name=ALOS_aspect_int@PERMANENT

r.out.gdal -c --overwrite input=ALOS_geomorfon_forms@PERMANENT output=/home/ivan/Projects/eco_maps/processed_maps/terrain/ALOS_geomorfon_forms.tiff format="GTiff" type="Byte" createopt="PROFILE=GeoTIFF,TFW=YES"

g.remove -f type=raster name=ALOS_merged_zero_water_smooth@PERMANENT
g.remove -f type=raster name=ALOS_merged_zero_water@PERMANENT
g.remove -f type=raster name=ALOS_geomorfon_forms@PERMANENT
g.remove -f type=raster name=ALOS_slope@PERMANENT
g.remove -f type=raster name=ALOS_aspect@PERMANENT
g.remove -f type=raster name=ALOS_curvature@PERMANENT
